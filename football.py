#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Soccer League Table ('http://codekata.com/data/04/football.dat')

The file football.dat contains the results from the English Premier League for
2001/2. The columns labeled ‘F’ and ‘A’ contain the total number of goals
scored for and against each team in that season (so Arsenal scored 79 goals
against opponents, and had 36 goals scored against them).

Your program should download the file and print the name of the team with the
smallest difference in ‘for’ and ‘against’ goals.
"""

import shutil
import sys

import pandas
import requests

import utils


def football_file_filter(idx, line):
    result = False
    if utils.has_alnum(line):
        result = True
    return result


def main():
    src_url = 'http://codekata.com/data/04/football.dat'
    dst = "football.dat"

    # get file
    try:
        dst = utils.download_file(src_url, dst)
    except requests.exceptions.RequestException as err:
        print("File {!r} couldn't be downloaded ({})".format(src_url, err))
        sys.exit(1)

    filtered_dst = "football-filtered.dat"
    utils.filter_file_lines(dst, filtered_dst, football_file_filter)

    # process file
    df = pandas.read_fwf(filtered_dst)
    df['goal_diff'] = df[['F']].subtract(df['A'], axis='index').abs()
    idx = df['goal_diff'].idxmin()
    row = df.iloc[idx]
    print("{} (The team with the smallest goal difference)".format(row['Team']))


if __name__ == "__main__":
    main()
