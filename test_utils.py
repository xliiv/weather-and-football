import io
import tempfile
import unittest
from requests.exceptions import HTTPError

import utils


class ResponseStub:
    raw = io.BytesIO(b"")
    status_code = 200

    def set_raw(self, bytes_content):
        self.raw = io.BytesIO(bytes_content)

    def raise_for_status(self):
        pass


class TestDownloadFile(unittest.TestCase):

    def setUp(self):
        self.dst = tempfile.NamedTemporaryFile(mode='r+')
        self.response = ResponseStub()

    def test_dst_is_saved_when_url_exists(self):
        content = "brand new content"
        self.response.set_raw(content.encode('utf8'))

        utils._download_file(self.response, self.dst.name)

        self.dst.seek(0)
        self.assertEqual(self.dst.read(), content)


class TestFilterFileLines(unittest.TestCase):

    def setUp(self):
        self.src = tempfile.NamedTemporaryFile(mode='r+')
        self.dst = tempfile.NamedTemporaryFile(mode='r')

    def _write_src_content(self, content):
        self.src.seek(0)
        self.src.write(content)
        self.src.seek(0)

    def test_dst_file_contains_only_filtered_lines(self):
        digit_line = "1234567890"
        blank_line = ""
        self._write_src_content('\n'.join([digit_line, blank_line]))

        utils.filter_file_lines(self.src.name, self.dst.name, lambda i, t: len(t) > 0 )

        with open(self.dst.name) as dst:
            self.assertEqual(dst.read(), digit_line + '\n')


class TestHasAlNum(unittest.TestCase):

    def test_false_when_only_spaces(self):
        self.assertEqual(utils.has_alnum('    '), False)

    def test_false_when_mix_dash_and_spaces(self):
        self.assertEqual(utils.has_alnum('  ---   '), False)

    def test_true_when_has_alpha(self):
        self.assertEqual(utils.has_alnum('  -a-   '), True)

    def test_true_when_has_digit(self):
        self.assertEqual(utils.has_alnum('  -4-   '), True)

