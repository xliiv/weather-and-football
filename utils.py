#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import shutil

import requests


def _download_file(request_response, dst):
    """Downloads stored in `request_response` stream and stores it at `dst`."""
    request_response.raise_for_status()
    if request_response.status_code == 200:
        with open(dst, 'wb') as f:
            request_response.raw.decode_content = True
            shutil.copyfileobj(request_response.raw, f)
        return dst


def download_file(url, dst):
    """Downloads file from `url` and stores it at `dst`."""
    return _download_file(requests.get(url, stream=True), dst)


def filter_file_lines(src_path, dst_path, filter_fn):
    """
    Copies filtered (by `filter_fn) lines from `src_path` to `dst_path` 
    """
    with open(src_path) as src_h:
        with open(dst_path, 'w') as dst_h:
            for idx, line in enumerate(src_h.readlines()):
                if filter_fn(idx, line):
                    dst_h.write(line)


def has_alnum(text):
    """Returns True if text contains any alphanumeric chars"""
    return any(char.isalnum() for char in text)
