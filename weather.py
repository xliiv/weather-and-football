#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
1. Weather Data ('http://codekata.com/data/04/weather.dat')

In weather.dat you’ll find daily weather data for Morristown, NJ for June 2002.
Your program downloads this text file then outputs the day number (column one)
with the smallest temperature spread (the maximum temperature is the second
column, the minimum the third column).
"""

import shutil
import sys

import pandas
import requests

import utils


def weather_file_filter(idx, line):
    result = False
    if utils.has_alnum(line):
        result = True
    return result


def main():
    src_url = 'http://codekata.com/data/04/weather.dat'
    dst = "weather.dat"

    # get file
    try:
        utils.download_file(src_url, dst)
    except requests.exceptions.RequestException as err:
        print("File {!r} couldn't be downloaded ({})".format(src_url, err))
        sys.exit(1)

    filtered_dst = "weather-filtered.dat"
    utils.filter_file_lines(dst, filtered_dst, weather_file_filter)

    # process file
    df = pandas.read_fwf(filtered_dst)
    df.MxT.replace({r'[*]':''}, regex=True, inplace=True)
    df.MnT.replace({r'[*]':''}, regex=True, inplace=True)
    df[['MxT','MnT']] = df[['MxT','MnT']].apply(pandas.to_numeric)
    df['spread'] = df[['MxT']].subtract(df['MnT'], axis='index')
    idx = df['spread'].idxmin()
    row = df.iloc[idx]
    print("{} (The day number with the smallest temperature spread)".format(row['Dy']))


if __name__ == "__main__":
    main()
