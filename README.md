## Instalation

```bash
# you may want to create virtualenv before installation, like
# virtualenv -p python3 _venv
# and source it by
# source _venv/bin/activate

pip install -r requirements.txt
```

## Running

This project includes two scripts:

### Weather

```bash
python3 weather.py
```

### Football

```bash
python3 football.py
```
